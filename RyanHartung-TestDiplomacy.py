#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, Army

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(str(a), "A Madrid")

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        a = diplomacy_read(s)
        self.assertEqual(str(a), "B Madrid")

    def test_read_3(self):
        s = "E Austin Support A\n"
        a = diplomacy_read(s)
        self.assertEqual(str(a), "E Austin")

    # ----
    # eval
    # ----

    def test_eval_1(self):
        armies = [Army("A", "Madrid")]
        v = diplomacy_eval(armies)
        self.assertEqual(v, ["A Madrid"])

    def test_eval_2(self):
        armies = [Army("A", "Madrid"), Army("B", "Madrid"), Army("C", "London", "B")]
        v = diplomacy_eval(armies)
        results = ["A [dead]","B Madrid","C London"]
        self.assertEqual(v, results)

    def test_eval_3(self):
        armies = [Army("A", "Madrid"), Army("B", "Madrid"), Army("C", "Madrid"), Army("D", "Paris", "B")]
        v = diplomacy_eval(armies)
        results = ["A [dead]", "B Madrid", "C [dead]", "D Paris"]
        self.assertEqual(v, results)

    def test_eval_4(self):
        armies = [Army("A", "Madrid"), Army("B", "Madrid"), Army("C", "Madrid"), Army("D", "Paris", "B"), Army("E", "Austin", "A")]
        v = diplomacy_eval(armies)
        results = ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"]
        self.assertEqual(v, results)

    def test_eval_5(self):
        armies = [Army("A", "Madrid"), Army("B", "Madrid"), Army("C", "Madrid")]
        v = diplomacy_eval(armies)
        results = ["A [dead]", "B [dead]", "C [dead]"]
        self.assertEqual(v, results)

    def test_eval_6(self):
        armies = [Army("A", "Madrid"), Army("B", "Austin"), Army("C", "London")]
        v = diplomacy_eval(armies)
        results = ["A Madrid", "B Austin", "C London"]
        self.assertEqual(v, results)



    # -----
    # print
    # -----

    def test_print_1(self):
        results = ["A [dead]", "B Madrid", "C [dead]", "D Paris"]
        w = StringIO()
        diplomacy_print(w, results)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
        
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Houston Support A\nC Barcelona Move Madrid\nD London Move Madrid\nE Austin Move Madrid\nG Waco Move Austin\nF Dallas Support B\nH Orange Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Houston\nC [dead]\nD [dead]\nE [dead]\nF Dallas\nG Austin\nH Orange\n")

    def test_solve_3(self):
        r = StringIO("X Madrid Hold\nY Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "X [dead]\nY [dead]\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
"""
