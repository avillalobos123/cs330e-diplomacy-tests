#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

# if supporting army gets attacked
# if everyone moves into non-competitive city
# if multiple support
# if multiple move to same city
# if army supporting is supported and gets attacked
# if support a army supporting (does not stack support)
# test for moving an army into a city before any other is said to occupy it

class TestDiplomacy (TestCase):

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Houston Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE Houston\n")
        
    def test_solve_2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move London\nC London Move Austin\nD Austin Move Houston\nE Houston Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB London\nC Austin\nD Houston\nE Madrid\n")
            
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A \nC London Support A\nD Austin Move Madrid\nE Houston Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\nE Houston\n")
            
    def test_solve_4(self):
        r = StringIO("A Madrid Support D\nB Barcelona Support A\nC London Move Madrid\nD Austin Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC [dead]\nD Austin\n")
    
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Austin Move Madrid\nE Houston Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC London\nD [dead]\nE Houston\n")
    
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Paris Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE Paris\n")
        
    def test_solve_7(self):
        r = StringIO("A Austin Move Barcelona\nB Barcelona Hold\nC Chicago Move Dallas\nD Dallas Support E\nE Edinburgh Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE Edinburgh\n")


if __name__ == "__main__": #pragma: no cover
    main()
