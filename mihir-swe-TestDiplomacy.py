import unittest
from Diplomacy import diplomacy_solve, parse_input, diplomacy_write
from io import StringIO


# tests for all three functions (diplomacy.py)


class TestDiplomacy(unittest.TestCase):
    # Diplomacy.py tests, main function
    def test_example_1(self):
        data = [
            "A Madrid Hold",
        ]
        self.assertEqual(diplomacy_solve(data), ["A Madrid"])

    def test_example_2(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B Madrid", "C London"])

    def test_example_3(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B [dead]"])

    def test_example_4(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B",
            "D Austin Move London",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])

    def test_example_5(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B [dead]", "C [dead]"])

    def test_example_6(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
            "D Paris Support B",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B Madrid", "C [dead]", "D Paris"])

    def test_example_7(self):
        data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
            "D Paris Support B",
            "E Austin Support A",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"])

    # New test cases to cover 'Move' and 'Support' actions
    def test_move_without_opposition(self):
        data = [
            "A Madrid Move Barcelona",
        ]
        self.assertEqual(diplomacy_solve(data), ["A Barcelona"])

    def test_move_with_opposition(self):
        data = [
            "A Madrid Move Barcelona",
            "B Barcelona Hold",
        ]
        self.assertEqual(diplomacy_solve(data), ["A [dead]", "B [dead]"])

    def test_support_hold(self):
        data = [
            "A Madrid Hold",
            "B Zaragoza Support A",
        ]
        self.assertEqual(diplomacy_solve(data), ["A Madrid", "B Zaragoza"])

    def test_support_cut_by_move(self):
        data = [
            "A Madrid Hold",
            "B Zaragoza Support A",
            "C Barcelona Move Zaragoza",
        ]
        self.assertEqual(diplomacy_solve(data), ["A Madrid", "B [dead]", "C [dead]"])

    def test_support_cut_by_attack(self):
        data = [
            "A Madrid Hold",
            "B Zaragoza Support A",
            "C Barcelona Move Madrid",
        ]
        self.assertEqual(diplomacy_solve(data), ["A Madrid", "B Zaragoza", "C [dead]"])

    # parse input tests
    def test_parse_input_empty_list(self):
        data = []
        expected = []
        self.assertEqual(parse_input(data), expected)

    def test_parse_input_none_input(self):
        data = None
        self.assertIsNone(parse_input(data), None)

    def test_parse_input_incorrect_action(self):
        data = ["A Madrid Retreat Seville"]  # 'Retreat' is not a valid action
        self.assertIsNone(parse_input(data))

    def test_parse_input_extra_space(self):
        data = ["A  Madrid  Hold"]
        expected = [{'army': 'A', 'location': 'Madrid', 'action': 'Hold', 'target': None}]
        self.assertEqual(parse_input(data), expected)

    # diplomacy_write tests
    def test_write_simple_input(self):
        input_data = ["A Madrid Hold"]
        expected_output = "A Madrid\n"

        with StringIO() as fake_output:
            diplomacy_write(input_data, fake_output)
            self.assertEqual(fake_output.getvalue(), expected_output)

    def test_write_empty_input(self):
        input_data = []
        expected_output = ""

        with StringIO() as fake_output:
            diplomacy_write(input_data, fake_output)
            self.assertEqual(fake_output.getvalue(), expected_output)

    def test_write_multiple_input(self):
        input_data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid"
        ]
        expected_output = "A [dead]\nB [dead]\n"  # Corrected expected output to match diplomacy_solve output

        with StringIO() as fake_output:
            diplomacy_write(input_data, fake_output)
            self.assertEqual(fake_output.getvalue(), expected_output)


if __name__ == "__main__":
    unittest.main()
